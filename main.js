#!/usr/bin/env node
'use strict';

let options = '';
let command_args = ['casper.js']; 
let fs = require('fs');
let os = require('os');

let packageJson = require('./package.json');

// オプションをパース
let program = require('commander');
program
  .version(packageJson.version)
  .option('-d, --device [name]', 'Choose device settings from devices.json')
  .option('-u, --basic-id [basicId]', 'Username for Basic Authentication.')
  .option('-p, --basic-password [basicPassword]', 'Password for Basic Authentication.')
  .option('-o, --output-dir [outputDir]', 'Path to output image directory.')
  .option('-i, --input-file [inputFile]', 'Path to url list file.(*.cvs or *.json)')
  .parse(process.argv);

// デバイス設定を取得
if(program.device != null){
    options += ' --device=' + program.device;
    command_args.push('--device=' + program.device);
}

// usernameとpasswordが指定されていればBasic認証
let basicId = null;
let basicPass = null;
if(program.basicId != null && program.basicPassword != null){
    options += ' --id=' + program.basicId + ' --password=' + program.basicPassword;
    command_args.push('--id=' + program.basicId);
    command_args.push('--password=' + program.basicPassword);
}

// 保存先ディレクトリを取得
if(program.outputDir != null){
    fs.mkdir(program.outputDir, function(error){
        // ディレクトリがすでに存在していた場合の処理はここに書く
        return;
    });
    command_args.push('--output=' + program.outputDir);
}

// URLリストを取得
if(program.inputFile != null){
  try{
    fs.statSync(program.inputFile);
    command_args.push('--file=' + program.inputFile);
  }catch(error){
    console.error('File ' + program.inputFile + ' is not exists.');
    process.abort();
  }
}

// casperjsを実行
let command = null;
if(os.type().toString().match('Windows')){
    command = 'node_modules\\.bin\\casperjs.cmd';
}else{
    command = 'node_modules/.bin/casperjs';
}
const spawn = require('child_process').spawn;
const ls = spawn(command, command_args);

ls.stdout.on('data', (data) => {
  console.log(data.toString());
  //console.log(data.toString().replace(/\r?\n/g,""));
  //console.log(`stdout: ${data}`);
});

ls.stderr.on('data', (data) => {
  console.log(data.toString().replace(/\r?\n/g,""));
  //console.log(`stderr: ${data}`);
});

ls.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});
