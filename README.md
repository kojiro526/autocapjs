# autocapjs

URLのリストを元にスクリーンショットを取得するツールです。

ヘッドレスブラウザであるPhantomJSを利用して指定したWebページにアクセスし、スクリーンショットをとります。

## Features

- URLのリスト（CSVまたはJSON）を元に、コマンドラインからスクリーンショットをとることができます。
- 予め定義したデバイスリストを元に、画面のサイズやブラウザのユーザエージェントを切り替えることができます。
- 縦に長いページでも全体のスクリーンショットをとることが出来ます。
- Basic認証にも対応しています。

## Requirement

- 予めnodeがインストールされている必要があります。
- 予めPhantomJSがインストールされている必要があります。
    - http://phantomjs.org/
- Windows環境ではpythonがインストールされている必要があります。
    - https://www.python.org/

## Installing

パッケージとして登録されていないので、以下の手順でインストールして下さい。

~~~
// gitでリポジトリをcolone
$ git clone https://kojiro526@bitbucket.org/kojiro526/autocapjs.git

// cloneしたディレクトリに移動
$ cd autocapjs

// npmコマンドで依存モジュールをインストール
$ npm install

// autocapコマンドをグローバルに使えるようにする
$ npm link

// 以下のようにコマンドを実行できれば成功です。
$ autocap -V
0.1.0
~~~

## Usage

`autocap --help`でヘルプを確認出来ます。

~~~
  Usage: autocap [options]

  Options:

    -h, --help                            output usage information
    -V, --version                         output the version number
    -d, --device [name]                   Choose device settings from devices.json
    -u, --basic-id [basicId]              Username for Basic Authentication.
    -p, --basic-password [basicPassword]  Password for Basic Authentication.
    -o, --output-dir [outputDir]          Path to output image directory.
    -i, --input-file [inputFile]          Path to url list file.(*.cvs or *.json)
~~~

### `-d --device [name]`

device.jsonファイル内に定義されたデバイス名を指定します。

device.jsonファイルには、予め以下のようなデバイスが登録されています。
~~~
{
    "iPhone": {
        "width": 320,
        "height": 568,
        "agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A405 Safari/600.1.4"
    },
    "iPad": {
        "width": 768,
        "height": 1024,
        "agent": "Mozilla/5.0 (iPad; CPU OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A405 Safari/600.1.4"
    },
    "safari": {
        "width": 1280,
        "height": 900,
        "agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.63 Safari/537.36"
    }
}
~~~

`-d iPhone`のように指定すると、iPhoneの画面サイズ、UserAgentでアクセスします。

何も指定しなかった場合は以下の設定がデフォルト値として使用されます。

- width
    - 1280
- height
    - 768
- agent
    - Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.63 Safari/537.36

### `-u --basic-id [basicId]`

Basic認証のIDを指定できます。

### `-p --basic-password [basicPassword]`

Basic認証のパスワードを指定できます。

### `-o --output-dir [outputDir]`

キャプチャした画像ファイルを保存するディレクトリのパスを指定できます。

ディレクトリが存在しない場合は自動的に作成されます。

### `-i --input-fiile [inputFile]`

URLのリストを定義したファイルを指定します。

URLリストのファイル名は`*.csv`または`*.json`である必要があります。

#### CSV形式の場合

以下の形式で記載して下さい。
~~~
[url], [画像ファイル名]
~~~

例： list.csv
~~~
http://yahoo.co.jp, yahoo
http://google.com, google
~~~

#### JSON形式の場合

以下の形式で記載して下さい。
~~~
[
    [url, 画像ファイル名],
    [url, 画像ファイル名],
    ...
]
~~~

例： list.json
~~~
[
    [http://yahoo.co.jp, yahoo],
    [http://google.com, google]
]
~~~

## Example

以下のようにコマンドを実行すれば、指定したディレクトリにPNG画像でスクリーンショットが保存されます。
~~~
$ autocap -i list.csv -o ./screen_shot -d iPhone
$ ls  ./screen_shot
yahoo.png     google.png
~~~
