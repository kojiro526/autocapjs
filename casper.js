var casper = require('casper').create({clientScripts: ['./lib/jquery-1.11.3.js']});
var fs = require('fs');
var waitTime = 2000;

// urlリストファイルの読み込み
var listFile = casper.cli.options.file;
console.log("Load url list from " + listFile);

var urls = [];
if(listFile.match(/\.json$/) != null){
    urls = require(listFile);
}
if(listFile.match(/\.csv$/) != null){
    var text = fs.read(listFile, 'utf8');
    var lines = text.split(/\r\n|\r|\n/);
    lines.forEach(function(line){
        urls.push(line.replace(/\r\n|\r|\n/, '').split(/,/));
    });
}
if(urls.count == 0){
    console.error('Nothing url in file.');
    exit();
}

// デバイスリストの読み込み
var devices = JSON.parse(fs.read('devices.json', 'utf8'));

// デバイス設定を取得
var deviceName = casper.cli.options.device;
var device = null;
if(deviceName == null){
    // 指定が無い場合はデフォルト設定を取得
    device = {
        "width": 1280,
        "height": 768,
        "agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.63 Safari/537.36"
    };
}else{
    if(Object.keys(devices).indexOf(deviceName) >= 0){
        device = devices[deviceName];
    }else{
        console.error('%s is unknown device name.', deviceName);
        process.exit();
    }
}
console.log('Device: ' + deviceName);
console.log('  width : ' + device.width);
console.log('  height: ' + device.height);
console.log('  agent : ' + device.agent);

// Basic認証設定
var basicId = null;
var basicPass = null;
if(casper.cli.options.id != null && casper.cli.options.password != null){
    basicId = casper.cli.options.id;
    basicPass = casper.cli.options.password;
}

// 保存先ディレクトリの取得
var outputDir = '';
if(casper.cli.options.output != null){
    // 末尾にスラッシュがある場合は削除
    outputDir = casper.cli.options.output.replace(/\/$/, "");
}

// スクレイピング実行
casper.start();
casper.userAgent(device.agent);
casper.viewport(device.width, device.height);
casper.setHttpAuth(basicId, basicPass);

var count = 0;
casper.each(
    urls,
    function(self,row){
        var url = row[0].replace(/^\s*/, '').replace(/\s*$/, '');
        if(url == '') return;
        // ファイル名が指定されていればその名前で保存する。
        var file = null;
        if(row[1] != null && row[1] != ''){
            file = row[1].replace();
            file = file.replace(/^\s*/, '').replace(/\s*$/, '');
        }else{
            file = count;
        }
        if(outputDir != '' && outputDir != null){
            file = outputDir + '/' + file;
        }
        file = file + '.png';
        self.thenOpen(url, function () {
            this.echo(
                "captur url : " + url + "\n" +
                "captur page title : " + this.getTitle() + "\n" +
                "outpu file : " + file + "\n\n"
             );
            casper.evaluate(function(){
                $('body').css('background-color','#fff');
            });
        });

        self.wait(waitTime,function(){
            self.capture(file);
        });

        count += 1;
    }
);
casper.run();
